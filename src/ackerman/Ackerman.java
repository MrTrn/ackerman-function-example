/*
 * Ch15: recursion: Ackerman function (prob.4 from Ch.15 notes)
 * Terje Rene E. Nilsen
 * tnilsen@my.hpu.edu
 */
package ackerman;

import java.util.Scanner;

/**
 *
 * @author rene
 */
public class Ackerman {

    public static double ackermann(double m, double n) {
      if (m == 0) { System.out.println("m=0 "  + " n+1: " + (n+1)); return n+1; }
      if (n == 0) return ackermann(m-1, 1);
      System.out.println("m: " + m + " n: " + n);
      return ackermann(m-1, ackermann(m, n-1));
   }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         
        Scanner keym = new Scanner(System.in); // create scanner obj for keyboard input
        Scanner keyn = new Scanner(System.in); // create scanner obj for keyboard input  
        // TODO code application logic here
        System.out.println("Ancerman(m,n) " );
        System.out.println("Enter m and n seperated by 'enter/return': " );
        System.out.println("Result: " + ackermann(keym.nextDouble(), keyn.nextDouble()));
    }
}
